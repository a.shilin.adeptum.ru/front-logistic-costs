import React from 'react';
import { Button } from '@consta/uikit/Button';
import logo from '../../assets/logo.svg';
import { Link } from 'react-router-dom';

import styles from './styles.styl';
import cn from 'classnames/bind';

const cx = cn.bind(styles);

const MainPage = () => (
  <div className={cx('App')}>
    <header className={cx('App-header')}>
      <img src={logo} className={cx('App-logo')} alt="  logo" />
      <p>
        Edit <code> src / App.tsx </code> and save to reload.
      </p>
      <Link to={'/user'}>
        <Button className={cx('App-button')} label="User" />
      </Link>
    </header>
  </div>
);

export default MainPage;
