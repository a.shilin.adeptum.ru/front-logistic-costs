import React from 'react';
import './styles.styl';
import { Table } from '@consta/uikit/Table';
import { useState } from 'react';

const User = () => {
  const columns: any = [
    {
      title: 'Месторождение',
      accessor: 'field',
      align: 'left',
      sortable: true,
    },
    {
      title: 'Год открытия',
      accessor: 'year',
      align: 'center',
      sortable: true,
    },
    {
      title: 'Тип',
      accessor: 'type',
      align: 'left',
    },
    {
      title: 'Полные запасы, млн. т.',
      accessor: 'estimatedReserves',
      align: 'right',
      sortable: true,
    },
    {
      title: 'Остаточные извлекаемые запасы, млн. т.',
      accessor: 'remainingReserves',
      align: 'right',
      sortable: true,
    },
    {
      title: 'Добыча тыс. т/сут.',
      accessor: 'production',
      align: 'right',
      sortable: true,
    },
    {
      title: 'Всего добыто, млн. т.',
      accessor: 'total',
      align: 'right',
      sortable: true,
    },
  ];

  const rows = [
    {
      id: 'row1',
      field: 'Северное',
      year: 1982,
      type: 'Нефть',
      estimatedReserves: 5000,
      remainingReserves: 1700,
      production: 33,
      total: 313,
      rows: [
        {
          id: 'row1.1',
          field: 'Северо-Западное',
          year: 1985,
          type: 'Конденсат',
          estimatedReserves: 2000,
          remainingReserves: 1300,
          production: 320,
          total: 143,
        },
        {
          id: 'row1.2',
          field: 'Северо-Восточное',
          year: 1983,
          type: 'Конденсат',
          estimatedReserves: 3000,
          remainingReserves: 1000,
          production: 310,
          total: 140,
        },
      ],
    },
    {
      id: 'row2',
      field: 'Южное',
      year: 2001,
      type: 'Конденсат',
      estimatedReserves: 7540,
      remainingReserves: 7540,
      production: 363,
      total: 88,
    },
    {
      id: 'row3',
      field: 'Западное',
      year: 1985,
      type: 'Комбинированное',
      estimatedReserves: 8766,
      remainingReserves: 3374,
      production: 256,
      total: 434,
    },
    {
      id: 'row4',
      field: 'Восточное',
      year: 1989,
      type: 'Конденсат',
      estimatedReserves: 1697,
      remainingReserves: 4818,
      production: 321,
      total: 236,
      rows: [
        {
          id: 'row4.1',
          field: 'Восточное-1',
          year: 1989,
          type: 'Конденсат',
          estimatedReserves: 1697,
          remainingReserves: 4818,
          production: 452,
          total: 819,
        },
        {
          id: 'row4.2',
          field: 'Восточное-2',
          year: 1989,
          type: 'Конденсат',
          estimatedReserves: 680,
          remainingReserves: 5321,
          production: 90,
          total: 112,
          rows: [
            {
              id: 'row4.2.1',
              field: 'Восточное-3',
              year: 1989,
              type: 'Конденсат',
              estimatedReserves: 1203,
              remainingReserves: 9832,
              production: 511,
              total: 295,
              rows: [
                {
                  id: 'row4.2.1.1',
                  field: 'Восточное-8',
                  year: 1989,
                  type: 'Конденсат',
                  estimatedReserves: 1203,
                  remainingReserves: 9832,
                  production: 293,
                  total: 222,
                },
                {
                  id: 'row4.2.1.2',
                  field: 'Восточное-7',
                  year: 1993,
                  type: 'Нефть',
                  estimatedReserves: 1203,
                  remainingReserves: 9832,
                  production: 250,
                  total: 236,
                },
              ],
            },
            {
              id: 'row4.2.2',
              field: 'Восточное-4',
              year: 2001,
              type: 'Нефть',
              estimatedReserves: 2121,
              remainingReserves: 1253,
              production: 411,
              total: 334,
            },
          ],
        },
      ],
    },
    {
      id: 'row5',
      field: 'Центральное',
      year: 1997,
      type: 'Нефть',
      estimatedReserves: 5169,
      remainingReserves: 3712,
      production: 292,
      total: 417,
    },
  ];
  const [visibleTooltip, setVisibleTooltip] = useState('');

  return (
    <div className="User">
      <Table
        columns={columns}
        rows={rows}
        activeRow={{
          id: visibleTooltip,
          onChange: ({ id }) => {
            setVisibleTooltip(id!);
          },
        }}
      />
    </div>
  );
};

export default User;
