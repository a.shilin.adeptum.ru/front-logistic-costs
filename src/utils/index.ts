export const updateAt = <T>(array: T[], index: number, newItem: T): T[] => {
  return index >= array.length
    ? array
    : [
        ...array.slice(0, index),
        newItem,
        ...array.slice(index + 1, array.length),
      ];
};
