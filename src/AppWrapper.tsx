import React from 'react';
import { hot } from 'react-hot-loader/root';
import './pages/MainPage/styles.styl';
import { Theme, presetGpnDefault } from '@consta/uikit/Theme';
import { BrowserRouter } from 'react-router-dom';
import App from './App';

const AppWrapper = () => {
  return (
    <Theme preset={presetGpnDefault}>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </Theme>
  );
};

export default hot(AppWrapper);
