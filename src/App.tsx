import React from 'react';
import './pages/MainPage/styles.styl';
import { Route, Switch } from 'react-router-dom';
import MainPage from './pages/MainPage';
import User from './pages/User';

const App = () => {
  return (
    <Switch>
      <Route path="/" exact component={MainPage} />
      <Route path="/user" component={User} />
    </Switch>
  );
};

export default App;
